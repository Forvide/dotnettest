﻿using System;
using System.ComponentModel.DataAnnotations;

namespace dotnetTest.Data.Entities
{
    public class AppUser
    {
        public virtual Guid Id { get; set; }
        [Required]
        public virtual string Iin { get; set; }
        [Required]
        public virtual string FirstName { get; set; }
        [Required]
        public virtual string LastName { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public virtual DateTime BirthDate { get; set; }
    }
}
