﻿using AutoMapper;
using dotnetTest.Data.Entities;
using dotnetTest.Models;

namespace dotnetTest.Data
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<AppUser, AppUserDto>();
            CreateMap<AppUserDto, AppUser>();
        }
    }
}
