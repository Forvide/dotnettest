﻿using dotnetTest.Services;

namespace dotnetTest.Data
{
    public class Admin
    {
        public string Login { get; set; } = "Admin";
        public string Password { get; set; } = "TempPass";
        public string Token { get; set; }
    }
}
