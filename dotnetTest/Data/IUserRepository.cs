﻿using dotnetTest.Data.Entities;
using dotnetTest.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace dotnetTest.Data
{
    public interface IUserRepository
    {
        Task<IEnumerable<AppUser>> GetUsersAsync();
        Task<AppUser> GetUserByIinAsync(string iin);
        Task<AppUserDto> CreateUserAsync(AppUserDto user);
        Task UpdateUserAsync(AppUserDto user);
        Task DeleteUserAsync(AppUser user);
        Task<bool> UserExistsAsync(string iin);
        Task<AppUser> GetUserByIdAsync(Guid id);
        Task<IEnumerable<AppUserDto>> GetUsersDtoAsync();
        Task<AppUserDto> GetSingleUserDtoAsync(string iin);
    }
}