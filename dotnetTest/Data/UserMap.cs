﻿using dotnetTest.Data.Entities;
using NHibernate;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using System;

namespace dotnetTest.Data
{
    public class UserMap : ClassMapping<AppUser>
    {
        public UserMap()
        {
            Id(x => x.Id, x =>
            {
                x.Generator(Generators.Guid);
                x.Type(NHibernateUtil.Guid);
                x.Column("Id");
                x.UnsavedValue(Guid.Empty);
            });

            Property(p => p.Iin, x =>
            {
                x.Length(12);
                x.Type(NHibernateUtil.StringClob);
                x.NotNullable(true);
            });

            Property(p => p.FirstName, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.StringClob);
                x.NotNullable(true);
            });

            Property(p => p.LastName, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.StringClob);
                x.NotNullable(true);
            });

            Property(p => p.BirthDate, x =>
            {
                x.Length(50);
                x.Type(NHibernateUtil.DateTime);
                x.NotNullable(true);
            });

            Table("Users");
        }
    }
}
