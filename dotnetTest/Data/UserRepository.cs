﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using dotnetTest.Data.Entities;
using dotnetTest.Models;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnetTest.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly IMapperSession _session;
        private readonly IMapper _mapper;

        public UserRepository(IMapperSession session, IMapper mapper)
        {
            _session = session;
            _mapper = mapper;
        }
        public async Task<bool> UserExistsAsync(string iin)
        {
            return await _session.AppUsers.AnyAsync(x => x.Iin == iin);
        }

        public async Task<AppUser> GetUserByIdAsync(Guid id)
        {
            return await _session.AppUsers.Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<AppUser>> GetUsersAsync()
        {
            return await _session.AppUsers.ToListAsync();
        }

        public async Task<AppUser> GetUserByIinAsync(string iin)
        {
            return await _session.AppUsers.Where(u => u.Iin == iin).FirstOrDefaultAsync();
        }

        public async Task<AppUserDto> CreateUserAsync(AppUserDto user)
        {        
            _session.BeginTransaction();
            var newUser = new AppUser()
            {
                Iin = user.Iin,
                FirstName = user.FirstName,
                LastName = user.LastName,
                BirthDate = user.BirthDate
            };

            await _session.Save(newUser);
            await _session.Commit();
            _session.CloseTransaction();

            return user;
        }

        public async Task UpdateUserAsync(AppUserDto user)
        {
            var targetedUser = await GetUserByIinAsync(user.Iin);

            targetedUser.Iin = user.Iin;
            targetedUser.FirstName = user.FirstName;
            targetedUser.LastName = user.LastName;
            targetedUser.BirthDate = user.BirthDate;

            _session.BeginTransaction();
            await _session.Save(targetedUser);
            await _session.Commit();
            _session.CloseTransaction();
        }

        public async Task DeleteUserAsync(AppUser user)
        {
            _session.BeginTransaction();
            await _session.Delete(user);
            await _session.Commit();
            _session.CloseTransaction();
        }

        public async Task<IEnumerable<AppUserDto>> GetUsersDtoAsync()
        {
            return await _session.AppUsers
                .ProjectTo<AppUserDto>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<AppUserDto> GetSingleUserDtoAsync(string iin)
        {
            return await _session.AppUsers
                .Where(x => x.Iin == iin)
                .ProjectTo<AppUserDto>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();
        }
    }
}
