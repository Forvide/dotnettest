﻿using System;

namespace dotnetTest.Models
{
    public class AppUserDto
    {
        public virtual string Iin { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime BirthDate { get; set; }
    }
}
