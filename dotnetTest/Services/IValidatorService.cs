﻿using System.Threading.Tasks;

namespace dotnetTest.Services
{
    public interface IValidatorService
    {
        bool ValidateIin(string iin);
        bool ValidateNames(string name);
    }
}
