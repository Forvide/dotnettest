﻿using dotnetTest.Data;

namespace dotnetTest.Services
{
    public interface ITokenService
    {
        string CreateToken(Admin admin);
    }
}
