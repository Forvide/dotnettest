﻿using System.Linq;
using System.Threading.Tasks;

namespace dotnetTest.Services
{
    public class ValidatorService : IValidatorService
    {
        public bool ValidateIin(string iin)
        {
            return string.IsNullOrEmpty(iin) || !iin.All(char.IsDigit) || iin.Length != 12;
        }

        public bool ValidateNames(string name)
        {
            return string.IsNullOrEmpty(name) || !name.All(char.IsLetterOrDigit);
        }
    }
}
