﻿using dotnetTest.Data;
using dotnetTest.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dotnetTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ITokenService _tokenService;

        public AccountController(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        // POST /api/<UserController>/login
        [HttpPost("login")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status401Unauthorized)]
        public ActionResult<string> GetToken(LoginForm loginForm)
        {
            var admin = new Admin();
            if (admin.Login != loginForm.Login || admin.Password != loginForm.Password)
            {
                return Unauthorized();
            }

            admin.Token = _tokenService.CreateToken(admin);
            return admin.Token;
        }
    }
}
