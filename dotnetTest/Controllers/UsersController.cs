﻿using dotnetTest.Data;
using dotnetTest.Models;
using dotnetTest.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace dotnetTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IValidatorService _validatorService;

        public UsersController(IUserRepository userRepository, IValidatorService validatorService)
        {
            _userRepository = userRepository;
            _validatorService = validatorService;
        }

        // GET: api/<UsersController>
        [HttpGet]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<AppUserDto>>> Get()
        {
            var users = await _userRepository.GetUsersDtoAsync();
            return Ok(users);
        }

        // GET api/<UsersController>/5
        [HttpGet("{iin}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<AppUserDto>> Get(string iin)
        {
            if (_validatorService.ValidateIin(iin))
                return BadRequest("IIN must only contain digits and have length of 12");

            var user = await _userRepository.GetSingleUserDtoAsync(iin);
            if (user == null) return NotFound("Such user not found");
            return Ok(user);
        }

        // POST api/<UsersController>
        [HttpPost]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<AppUserDto>> Post(AppUserDto user)
        {
            if (_validatorService.ValidateIin(user.Iin))
                return BadRequest("IIN must only contain digits and have length of 12");

            if (_validatorService.ValidateNames(user.FirstName) || 
                _validatorService.ValidateNames(user.LastName))
                return BadRequest("Names must only contain alphanumeric symbols");

            if (await _userRepository.UserExistsAsync(user.Iin))
                return BadRequest("This person already registered.");
            var userToSend = await _userRepository.CreateUserAsync(user);
            return Ok(userToSend);
        }

        // PUT api/<UsersController>/5
        [HttpPut]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Put(AppUserDto user)
        {
            if (_validatorService.ValidateIin(user.Iin))
                return BadRequest("IIN must only contain digits and have length of 12");

            if (_validatorService.ValidateNames(user.FirstName) ||
                _validatorService.ValidateNames(user.LastName))
                return BadRequest("Names must only contain alphanumeric symbols");
            var targetedUser = _userRepository.GetUserByIinAsync(user.Iin);
            if (await targetedUser == null)
                return NotFound("User wasn't found");

            await _userRepository.UpdateUserAsync(user);
            return Ok();
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{iin}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Delete(string iin)
        {
            if (_validatorService.ValidateIin(iin))
                return BadRequest("IIN must only contain digits and have length of 12");
            var user = await _userRepository.GetUserByIinAsync(iin);
            if (user == null)
                return NotFound("User wasn't found");
            await _userRepository.DeleteUserAsync(user);
            return Ok();
        }
    }
}
