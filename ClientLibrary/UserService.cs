﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;

namespace ClientLibrary
{
    public class UserService
    {
        private readonly HttpClient _client;

        public UserService(HttpClient client)
        {
            _client = client;
        }

        public void GetUsers(string token)
        {
            var response = _client.GetAsync("/api/Users").GetAwaiter().GetResult();
            var content = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var users = JsonSerializer.Deserialize<IEnumerable<AppUser>>(content);
            foreach (var user in users)
            {
                Console.WriteLine($"IIN: {user.iin}, FirstName: {user.firstName}, " +
                    $"LastName: {user.lastName}, DateOfBirth: {user.birthDate}" + Environment.NewLine);
            }
        }

        public void GetUserByIin(string token)
        {
            Console.WriteLine("Enter IIN(Only 12 digits number is acceptable):");
            string iin = Console.ReadLine();
            var response = _client.GetAsync($"/api/Users/{iin}").GetAwaiter().GetResult();
            var content = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var user = JsonSerializer.Deserialize<AppUser>(content);
            Console.WriteLine("Persons IIN: " + user.iin);
            Console.WriteLine("Persons First Name: " + user.firstName);
            Console.WriteLine("Persons Last Name: " + user.lastName);
            Console.WriteLine("Persons Date of birth: " + user.birthDate + Environment.NewLine);
        }

        public string PostUser(string token)
        {
            var user = GetAppUserInfo();
            var response = _client.PostAsync("/api/Users", user).GetAwaiter().GetResult();
            return response.ToString();
        }

        public string UpdateUser(string token)
        {
            var user = GetAppUserInfo();
            var response = _client.PutAsync("/api/Users", user).GetAwaiter().GetResult();
            return response.ToString();
        }

        public string DeleteUserByIin(string token)
        {
            Console.WriteLine("Enter IIN(Only 12 digits number is acceptable):");
            string iin = Console.ReadLine();
            Console.WriteLine("Confirm deletion (y/n):");
            string confirmation = Console.ReadLine();

            if (confirmation.ToLower().StartsWith("y"))
            {
                _ = _client.DeleteAsync($"/api/Users/{iin}").GetAwaiter().GetResult();
                return "User deleted";
            }
            return "Operation canceled";
        }

        public ByteArrayContent GetAppUserInfo()
        {
            Console.WriteLine("Enter IIN(Only 12 digits number is acceptable):");
            string iin = Console.ReadLine();

            Console.WriteLine("Enter First Name(Only alphanumeric):");
            string firstName = Console.ReadLine();

            Console.WriteLine("Enter Last Name(Only alphanumeric):");
            string lastName = Console.ReadLine();

            Console.WriteLine("Enter Date of birth(example: 01/01/2001):");
            DateTime birthDate = Convert.ToDateTime(Console.ReadLine());

            AppUser user = new AppUser
            {
                iin = iin,
                firstName = firstName,
                lastName = lastName,
                birthDate = birthDate
            };

            var content = JsonSerializer.Serialize(user);
            var buffer = System.Text.Encoding.UTF8.GetBytes(content);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return byteContent;
        }
    }
}
