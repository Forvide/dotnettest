﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace ClientLibrary
{
    class Program
    {
        private static string token;
        private static HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            Console.WriteLine("INFO=> Login: Admin, Password: TempPass");
            client.BaseAddress = new Uri("https://localhost:44370");

            Dictionary<string, string> tokenDictionary = GetTokenDictionary();
            token = tokenDictionary["access_token"];

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);

            UserService userService = new UserService(client);

            bool toggle = true;
            while (toggle)
            {
                Console.WriteLine(Environment.NewLine + "Choose service: " +
                    "\n show_token | get_users | get_user_by_iin |" +
                    "\n add_new_user | edit_user | delete_user" +
                    "\n exit |" + Environment.NewLine);

                string service = Console.ReadLine();

                switch (service)
                {
                    case "exit":
                        toggle = false;
                        Console.WriteLine("Press any key to exit" + Environment.NewLine);
                        break;
                    case "show_token":
                        Console.WriteLine("\nAccess Token:");
                        Console.WriteLine(token + Environment.NewLine);
                        break;
                    case "get_users":
                        userService.GetUsers(token);
                        Console.WriteLine();
                        break;
                    case "get_user_by_iin":
                        userService.GetUserByIin(token);
                        Console.WriteLine();
                        break;
                    case "add_new_user":
                        Console.WriteLine();
                        string postUser = userService.PostUser(token);
                        Console.WriteLine(postUser + Environment.NewLine);
                        break;
                    case "edit_user":
                        Console.WriteLine();
                        string updUser = userService.UpdateUser(token);
                        Console.WriteLine(updUser + Environment.NewLine);
                        break;
                    case "delete_user":
                        Console.WriteLine();
                        string delUser = userService.DeleteUserByIin(token);
                        Console.WriteLine(delUser + Environment.NewLine);
                        break;
                    default:
                        Console.WriteLine("No such service" + Environment.NewLine);
                        break;
                }
            }

            Console.Read();
        }

        static Dictionary<string, string> GetTokenDictionary()
        {
            bool isAuthorised = false;
            var response = new HttpResponseMessage();
            while(!isAuthorised)
            {
                Console.WriteLine("Enter login:");
                string userName = Console.ReadLine();

                Console.WriteLine("Enter password:");
                string password = Console.ReadLine();

                LoginForm loginForm = new LoginForm
                {
                    Login = userName,
                    Password = password
                };

                var content = JsonSerializer.Serialize(loginForm);
                var msg = new HttpRequestMessage(HttpMethod.Post, "/api/account/login");
                msg.Content = new StringContent(content, Encoding.UTF8, "application/json");

                response = client.SendAsync(msg).GetAwaiter().GetResult();
                if (response.StatusCode.ToString() == "OK")
                {
                    isAuthorised = true;
                }
                else
                {
                    Console.WriteLine("Login or Password is not valid");
                }
            }
           
            var result = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            Dictionary<string, string> tokenDictionary = new Dictionary<string, string>();
            tokenDictionary.Add("access_token", result);
            return tokenDictionary;
        }
    }
}