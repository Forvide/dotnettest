﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClientLibrary
{
    public class AppUser
    {
        public string iin { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime birthDate { get; set; }
    }
}